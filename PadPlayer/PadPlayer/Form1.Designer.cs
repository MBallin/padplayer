﻿namespace PadPlayer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonGo = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.outputListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.searchDepthLabel = new System.Windows.Forms.Label();
            this.buttonSolutionNext = new System.Windows.Forms.Button();
            this.buttonSolutionBack = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.comboBoxImageSelection = new System.Windows.Forms.ComboBox();
            this.labelImageSelection = new System.Windows.Forms.Label();
            this.numericUpDownSearchDepth = new System.Windows.Forms.NumericUpDown();
            this.labelHeuristic = new System.Windows.Forms.Label();
            this.comboBoxHeuristic = new System.Windows.Forms.ComboBox();
            this.labelMinCombo = new System.Windows.Forms.Label();
            this.comboBoxMinimumCombo = new System.Windows.Forms.ComboBox();
            this.labelColorPriority = new System.Windows.Forms.Label();
            this.comboBoxColorPriority = new System.Windows.Forms.ComboBox();
            this.numericUpDownBoardHeight = new System.Windows.Forms.NumericUpDown();
            this.labelBoardHeight = new System.Windows.Forms.Label();
            this.numericUpDownBoardWidth = new System.Windows.Forms.NumericUpDown();
            this.labelBoardWidth = new System.Windows.Forms.Label();
            this.panelCards = new System.Windows.Forms.Panel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSearchDepth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBoardHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBoardWidth)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonGo
            // 
            this.buttonGo.Location = new System.Drawing.Point(10, 472);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(100, 25);
            this.buttonGo.TabIndex = 0;
            this.buttonGo.Text = "GO!";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.go_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.ImageLocation = "F:\\Dropbox (Personal)\\PADPlayer\\screenshots";
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 300);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // outputListView
            // 
            this.outputListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.outputListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputListView.FullRowSelect = true;
            this.outputListView.GridLines = true;
            this.outputListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.outputListView.Location = new System.Drawing.Point(419, 12);
            this.outputListView.Name = "outputListView";
            this.outputListView.Size = new System.Drawing.Size(415, 346);
            this.outputListView.TabIndex = 11;
            this.outputListView.UseCompatibleStateImageBehavior = false;
            this.outputListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 0;
            // 
            // searchDepthLabel
            // 
            this.searchDepthLabel.AutoSize = true;
            this.searchDepthLabel.Location = new System.Drawing.Point(299, 415);
            this.searchDepthLabel.Name = "searchDepthLabel";
            this.searchDepthLabel.Size = new System.Drawing.Size(73, 13);
            this.searchDepthLabel.TabIndex = 15;
            this.searchDepthLabel.Text = "Search Depth";
            // 
            // buttonSolutionNext
            // 
            this.buttonSolutionNext.Location = new System.Drawing.Point(116, 472);
            this.buttonSolutionNext.Name = "buttonSolutionNext";
            this.buttonSolutionNext.Size = new System.Drawing.Size(100, 25);
            this.buttonSolutionNext.TabIndex = 17;
            this.buttonSolutionNext.Text = "Next Solution";
            this.buttonSolutionNext.UseVisualStyleBackColor = true;
            this.buttonSolutionNext.Click += new System.EventHandler(this.buttonSolutionNext_Click);
            // 
            // buttonSolutionBack
            // 
            this.buttonSolutionBack.Location = new System.Drawing.Point(222, 472);
            this.buttonSolutionBack.Name = "buttonSolutionBack";
            this.buttonSolutionBack.Size = new System.Drawing.Size(100, 25);
            this.buttonSolutionBack.TabIndex = 18;
            this.buttonSolutionBack.Text = "Previous Solution";
            this.buttonSolutionBack.UseVisualStyleBackColor = true;
            this.buttonSolutionBack.Click += new System.EventHandler(this.buttonSolutionBack_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // comboBoxImageSelection
            // 
            this.comboBoxImageSelection.FormattingEnabled = true;
            this.comboBoxImageSelection.Items.AddRange(new object[] {
            "Board Only",
            "Piece Finder",
            "Path Overlay"});
            this.comboBoxImageSelection.Location = new System.Drawing.Point(102, 366);
            this.comboBoxImageSelection.Name = "comboBoxImageSelection";
            this.comboBoxImageSelection.Size = new System.Drawing.Size(127, 21);
            this.comboBoxImageSelection.TabIndex = 19;
            this.comboBoxImageSelection.SelectedIndexChanged += new System.EventHandler(this.comboBoxImageSelection_SelectedIndexChanged);
            // 
            // labelImageSelection
            // 
            this.labelImageSelection.AutoSize = true;
            this.labelImageSelection.Location = new System.Drawing.Point(13, 369);
            this.labelImageSelection.Name = "labelImageSelection";
            this.labelImageSelection.Size = new System.Drawing.Size(80, 13);
            this.labelImageSelection.TabIndex = 20;
            this.labelImageSelection.Text = "Image Selectior";
            // 
            // numericUpDownSearchDepth
            // 
            this.numericUpDownSearchDepth.Location = new System.Drawing.Point(377, 413);
            this.numericUpDownSearchDepth.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDownSearchDepth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSearchDepth.Name = "numericUpDownSearchDepth";
            this.numericUpDownSearchDepth.Size = new System.Drawing.Size(33, 20);
            this.numericUpDownSearchDepth.TabIndex = 21;
            this.numericUpDownSearchDepth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSearchDepth.ValueChanged += new System.EventHandler(this.searchDepthUpDown_ValueChanged);
            // 
            // labelHeuristic
            // 
            this.labelHeuristic.AutoSize = true;
            this.labelHeuristic.Location = new System.Drawing.Point(45, 395);
            this.labelHeuristic.Name = "labelHeuristic";
            this.labelHeuristic.Size = new System.Drawing.Size(48, 13);
            this.labelHeuristic.TabIndex = 23;
            this.labelHeuristic.Text = "Heuristic";
            // 
            // comboBoxHeuristic
            // 
            this.comboBoxHeuristic.FormattingEnabled = true;
            this.comboBoxHeuristic.Items.AddRange(new object[] {
            "Mark",
            "Sean"});
            this.comboBoxHeuristic.Location = new System.Drawing.Point(102, 392);
            this.comboBoxHeuristic.Name = "comboBoxHeuristic";
            this.comboBoxHeuristic.Size = new System.Drawing.Size(127, 21);
            this.comboBoxHeuristic.TabIndex = 22;
            this.comboBoxHeuristic.SelectedIndexChanged += new System.EventHandler(this.comboBoxHeuristic_SelectedIndexChanged);
            // 
            // labelMinCombo
            // 
            this.labelMinCombo.AutoSize = true;
            this.labelMinCombo.Location = new System.Drawing.Point(13, 420);
            this.labelMinCombo.Name = "labelMinCombo";
            this.labelMinCombo.Size = new System.Drawing.Size(84, 13);
            this.labelMinCombo.TabIndex = 25;
            this.labelMinCombo.Text = "Minimum Combo";
            // 
            // comboBoxMinimumCombo
            // 
            this.comboBoxMinimumCombo.FormattingEnabled = true;
            this.comboBoxMinimumCombo.Items.AddRange(new object[] {
            "3+",
            "4+",
            "5+",
            "6+",
            "7+"});
            this.comboBoxMinimumCombo.Location = new System.Drawing.Point(102, 417);
            this.comboBoxMinimumCombo.Name = "comboBoxMinimumCombo";
            this.comboBoxMinimumCombo.Size = new System.Drawing.Size(127, 21);
            this.comboBoxMinimumCombo.TabIndex = 24;
            this.comboBoxMinimumCombo.SelectedIndexChanged += new System.EventHandler(this.comboBoxMinimumCombo_SelectedIndexChanged);
            // 
            // labelColorPriority
            // 
            this.labelColorPriority.AutoSize = true;
            this.labelColorPriority.Location = new System.Drawing.Point(28, 448);
            this.labelColorPriority.Name = "labelColorPriority";
            this.labelColorPriority.Size = new System.Drawing.Size(65, 13);
            this.labelColorPriority.TabIndex = 27;
            this.labelColorPriority.Text = "Color Priority";
            // 
            // comboBoxColorPriority
            // 
            this.comboBoxColorPriority.FormattingEnabled = true;
            this.comboBoxColorPriority.Items.AddRange(new object[] {
            "White",
            "Black",
            "Red",
            "Blue",
            "Green",
            "Heal",
            "Jammer",
            "Poision"});
            this.comboBoxColorPriority.Location = new System.Drawing.Point(102, 445);
            this.comboBoxColorPriority.Name = "comboBoxColorPriority";
            this.comboBoxColorPriority.Size = new System.Drawing.Size(127, 21);
            this.comboBoxColorPriority.TabIndex = 26;
            this.comboBoxColorPriority.SelectedIndexChanged += new System.EventHandler(this.comboBoxColorPriority_SelectedIndexChanged);
            // 
            // numericUpDownBoardHeight
            // 
            this.numericUpDownBoardHeight.Location = new System.Drawing.Point(377, 366);
            this.numericUpDownBoardHeight.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDownBoardHeight.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownBoardHeight.Name = "numericUpDownBoardHeight";
            this.numericUpDownBoardHeight.Size = new System.Drawing.Size(33, 20);
            this.numericUpDownBoardHeight.TabIndex = 29;
            this.numericUpDownBoardHeight.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownBoardHeight.ValueChanged += new System.EventHandler(this.numericUpDownBoardHeight_ValueChanged);
            // 
            // labelBoardHeight
            // 
            this.labelBoardHeight.AutoSize = true;
            this.labelBoardHeight.Location = new System.Drawing.Point(306, 368);
            this.labelBoardHeight.Name = "labelBoardHeight";
            this.labelBoardHeight.Size = new System.Drawing.Size(69, 13);
            this.labelBoardHeight.TabIndex = 28;
            this.labelBoardHeight.Text = "Board Height";
            // 
            // numericUpDownBoardWidth
            // 
            this.numericUpDownBoardWidth.Location = new System.Drawing.Point(377, 389);
            this.numericUpDownBoardWidth.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDownBoardWidth.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownBoardWidth.Name = "numericUpDownBoardWidth";
            this.numericUpDownBoardWidth.Size = new System.Drawing.Size(33, 20);
            this.numericUpDownBoardWidth.TabIndex = 31;
            this.numericUpDownBoardWidth.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownBoardWidth.ValueChanged += new System.EventHandler(this.numericUpDownBoardWidth_ValueChanged);
            // 
            // labelBoardWidth
            // 
            this.labelBoardWidth.AutoSize = true;
            this.labelBoardWidth.Location = new System.Drawing.Point(306, 391);
            this.labelBoardWidth.Name = "labelBoardWidth";
            this.labelBoardWidth.Size = new System.Drawing.Size(66, 13);
            this.labelBoardWidth.TabIndex = 30;
            this.labelBoardWidth.Text = "Board Width";
            // 
            // panelCards
            // 
            this.panelCards.Location = new System.Drawing.Point(419, 364);
            this.panelCards.Name = "panelCards";
            this.panelCards.Size = new System.Drawing.Size(415, 133);
            this.panelCards.TabIndex = 32;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 318);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(401, 40);
            this.progressBar1.TabIndex = 33;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(840, 507);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.panelCards);
            this.Controls.Add(this.numericUpDownBoardWidth);
            this.Controls.Add(this.labelBoardWidth);
            this.Controls.Add(this.numericUpDownBoardHeight);
            this.Controls.Add(this.labelBoardHeight);
            this.Controls.Add(this.labelColorPriority);
            this.Controls.Add(this.comboBoxColorPriority);
            this.Controls.Add(this.labelMinCombo);
            this.Controls.Add(this.comboBoxMinimumCombo);
            this.Controls.Add(this.labelHeuristic);
            this.Controls.Add(this.comboBoxHeuristic);
            this.Controls.Add(this.numericUpDownSearchDepth);
            this.Controls.Add(this.labelImageSelection);
            this.Controls.Add(this.comboBoxImageSelection);
            this.Controls.Add(this.buttonSolutionBack);
            this.Controls.Add(this.buttonSolutionNext);
            this.Controls.Add(this.searchDepthLabel);
            this.Controls.Add(this.outputListView);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonGo);
            this.Name = "MainForm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSearchDepth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBoardHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBoardWidth)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ListView outputListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Label searchDepthLabel;
        private System.Windows.Forms.Button buttonSolutionNext;
        private System.Windows.Forms.Button buttonSolutionBack;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ComboBox comboBoxImageSelection;
        private System.Windows.Forms.Label labelImageSelection;
        private System.Windows.Forms.NumericUpDown numericUpDownSearchDepth;
        private System.Windows.Forms.Label labelHeuristic;
        private System.Windows.Forms.ComboBox comboBoxHeuristic;
        private System.Windows.Forms.Label labelMinCombo;
        private System.Windows.Forms.ComboBox comboBoxMinimumCombo;
        private System.Windows.Forms.Label labelColorPriority;
        private System.Windows.Forms.ComboBox comboBoxColorPriority;
        private System.Windows.Forms.NumericUpDown numericUpDownBoardHeight;
        private System.Windows.Forms.Label labelBoardHeight;
        private System.Windows.Forms.NumericUpDown numericUpDownBoardWidth;
        private System.Windows.Forms.Label labelBoardWidth;
        private System.Windows.Forms.Panel panelCards;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}

