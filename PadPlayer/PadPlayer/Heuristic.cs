﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace PadPlayer
{
    public enum HeuristicType
    {
        Mark = 0,
        Sean = 1,
        Count = 2
    }
    class Heuristic
    {
        public struct multiplyer
        {
            public const double threeCombo = 1.0;
            public const double fourCombo = 1.25;
            public const double fiveCombo = 1.5;
            public const double sixCombo = 1.75;
            public const double sevenCombo = 2.0;
            public const double eightCombo = 2.25;
            public const double nineCombo = 3.0;
            public const double tenCombo = 3.25;
            public const double row = 1.0;
        }

        public static int comboMinimum { get; set; } = 3;

        public static int searchDepth { get; set; } = 5;

        public static PieceType colorPriority { get; set; } = PieceType.white;

        public static HeuristicType hueristic { get; set; } = HeuristicType.Mark;

        public static double runHeuristic(PieceType[,] state, HeuristicType heuristic)
        {
            if(hueristic == HeuristicType.Mark)
            {
                return HeuristicMark(state);
            }
            if (hueristic == HeuristicType.Mark)
            {
                return HeuristicSean(state);
            }
            return -1;
        }

        public static double HeuristicMark(PieceType[,] state)
        {
            int boardWidth = state.GetLength(0);
            int boardHeight = state.GetLength(1);
            int numOfThreeCombos = 0;
            int numOfFourCombos = 0;
            int numOfFiveCombos = 0;
            int numOfSixCombos = 0;
            int numOfSevenCombos = 0;
            int numOfEightCombos = 0;
            int numOfNineCombos = 0;
            int numOfTenCombos = 0;
            int rowCount = 1;
            int colCount = 1;
            PieceType rowType;
            PieceType colType;
            for (int i = 0; i < boardWidth; i++)
            {
                for (int j = 0; j < boardHeight - 1; j++)
                {
                    colType = state[i, j];
                    rowType = state[i, j];
                    if (state[i, j] == state[i, j+1] && colType == state[i, j])
                    {
                        colCount++;
                    }
                    else
                    {
                        colType = state[i, j+1];
                        colCount = 1;
                    }
                    if(i == 0)
                    {
                        for(int k = 0; k < boardWidth - 1; k++)
                        {
                            if (state[k,j] == state[k+1,j] && rowType == state[k,j])
                            {
                                rowCount++;
                            }
                            else
                            {
                                rowType = state[k + 1, j];
                                rowCount = 1;
                            }
                        }
                        if (rowCount == 3)
                        {
                            numOfThreeCombos++;
                        }
                        else if (rowCount == 4)
                        {
                            numOfFourCombos++;
                        }
                        else if (rowCount == 5)
                        {
                            numOfFiveCombos++;
                        }
                        else if (rowCount == 6)
                        {
                            numOfSixCombos++;
                        }
                        rowCount = 1;
                    }
                }
                if (colCount == 3)
                {
                    numOfThreeCombos++;
                }
                else if (colCount == 4)
                {
                    numOfFourCombos++;
                }
                else if (colCount == 5)
                {
                    numOfFiveCombos++;
                }
                else if (colCount == 6)
                {
                    numOfSixCombos++;
                }
                colCount = 1;
            }

            return  (numOfThreeCombos * multiplyer.threeCombo) + 
                    (numOfFourCombos * multiplyer.fourCombo) + 
                    (numOfFiveCombos * multiplyer.fiveCombo) + 
                    (numOfSixCombos * multiplyer.sixCombo) +
                    (numOfSevenCombos * multiplyer.sevenCombo) +
                    (numOfEightCombos * multiplyer.eightCombo) +
                    (numOfNineCombos * multiplyer.nineCombo) +
                    (numOfTenCombos * multiplyer.tenCombo);
        }

        public static double HeuristicSean(PieceType[,] board)
        {
            return -1;
        }

        /// <summary>
        /// Calculates heuristic of a state based on the tiles out of the place as compared with the goal state
        /// </summary>
        /// <param name="currentState"></param>
        /// <param name="goalState"></param>
        /// <returns>Total count of all tiles out of place</returns>
        public static int CountingTilesOutOfPlace(PieceType[,] currentState, PieceType[,] goalState)
        {
            int total = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (currentState[j, i] != goalState[j, i])
                    {
                        total++;
                    }
                }
            }
            return total;
        }

        public static bool StartOnTopOrBottom(PieceType[,] board)
        {
            PieceType[,] top = new PieceType[ImageController.BoardWidth,2];
            PieceType[,] bottom = new PieceType[ImageController.BoardWidth, 2];
            for (int i = 0; i < ImageController.BoardWidth; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    top[i, j] = board[i, j];
                }
            }
            for (int i = 0; i < ImageController.BoardWidth; i++)
            {
                for(int j = ImageController.BoardHeight - 2; j < ImageController.BoardHeight; j++)
                {
                    bottom[i, (j-3)] = board[i, j];
                }
            }
            return RegionScore(RegionCount(top)) > RegionScore(RegionCount(bottom));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="region"></param>
        /// <returns></returns>
        private static Dictionary<PieceType, int> RegionCount(PieceType[,] region)
        {
            Dictionary<PieceType, int> map = new Dictionary<PieceType, int>();
            for (int i = 0; i < region.GetLength(0); i++)
            {
                for (int j = 0; j < region.GetLength(1); j++)
                {
                    if (map.ContainsKey(region[i,j]))
                    {
                        map[region[i, j]]++;
                    }
                    else
                    {
                        map.Add(region[i,j], 1);
                    }
                }
            }
            return map.OrderBy(i => i.Value).ToDictionary(i => i.Key, i => i.Value);
        }

        public static int RegionScore(Dictionary<PieceType, int> pieces)
        {
            int score = 0;
            foreach(KeyValuePair<PieceType, int> piece in pieces)
            {
                int temp = piece.Value;
                while(temp >= 3)
                {
                    temp -= 3;
                    score++;
                }
            }
            return score;
        }

        public static List<PieceType[,]> RegionSort(PieceType[,] region)
        {
            List<PieceType[,]> sortedRegions = new List<PieceType[,]>();
            Dictionary<PieceType, int> regionCount = RegionCount(region);
            int regionScore = RegionScore(regionCount);
            PieceType[] piecesToSolve = regionCount.Keys.ToArray();
            // Horizontal sort when the region is wider than it is tall or the region is square
            if (region.GetLength(0) >= region.GetLength(1))
            {
                // Handle 6 x 2 region
                if(region.GetLength(0) == 6 && region.GetLength(1) == 2)
                {
                    foreach()
                    if(regionCount[piecesToSolve[0]] >= 3)
                    {
                        region[0, 1] = piecesToSolve[0];
                        region[1, 1] = piecesToSolve[0];
                        region[2, 1] = piecesToSolve[0];
                    }
                }
            }

            // Vertical sort when the region is taller than it is wide
            else
            {

            }

            return sortedRegions;
        }
    }
}
