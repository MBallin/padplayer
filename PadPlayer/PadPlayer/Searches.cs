﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PadPlayer
{
    class Searches
    {
        /// <summary>
        /// Implementaion of A* algorithm
        /// </summary>
        /// <param name="initialState"></param>
        /// <param name="goalBoard"></param>
        /// <param name="heuristic"></param>
        /// <returns>True: found a solution within 100 moves.  False otherwise.</returns>
        public static String A_Star(Board initialState, Board goalBoard, HeuristicType heuristic)
        {
            //Declare and initalize variables
            int numOfMoves = 0;
            Tree tree = new Tree(initialState, heuristic);
            tree.root.DistanceFromHome = 0;
            Node currentNode = tree.root;
            tree.openQueue.Enqueue(tree.root);

            while (currentNode.currentBoard != goalBoard && !tree.openQueue.IsEmpty)
            {
                //tree.bestPath = buildPath(tree.root, currentNode);
                numOfMoves = tree.bestPath.Count;

                //Expand node
                tree.createChildren(currentNode);
                tree.closedQueue.Enqueue(currentNode);
                foreach (Node n in currentNode.children)
                {
                    n.DistanceFromHome = currentNode.DistanceFromHome + 1;
                    n.HeuristicScore += n.DistanceFromHome;
                    if (!tree.closedQueue.Contains(n))
                    {
                        if (!tree.openQueue.Contains(n))
                        {
                            tree.openQueue.Enqueue(n);
                        }
                    }
                }

                if (!tree.openQueue.IsEmpty)
                {
                    //Move to best node of the children
                    currentNode = tree.openQueue.Dequeue();
                }

            }

            if (numOfMoves > 100)
            {
                return "No Solution found" + Environment.NewLine;
            }
            else
            {
                tree.bestPath = buildPath(tree.root, currentNode);
                return "NumOfMoves: " + tree.bestPath.Count + Environment.NewLine + PrintPath(tree.bestPath, 5);
            }
        }

        /// <summary>
        /// Implementaion of Breadth First Search algorithm
        /// </summary>
        /// <param name="initialState"></param>
        /// <param name="goalState"></param>
        /// <returns>True: found a solution within 100 moves.  False otherwise.</returns>
        static public void bfsSearch(Board initialState, Board goalState)
        {
            Tree tree = new Tree(initialState, HeuristicType.Count); // just added heuristic type randomly to provide an argument
            Node currentNode = tree.root;
            currentNode.HeuristicScore = 0;
            tree.openQueue.Enqueue(currentNode);

            // while the open queue isn't empty
            while (!tree.openQueue.IsEmpty && currentNode.HeuristicScore < 15)
            {
                // use level as the heuristic score for sorting
                currentNode = tree.openQueue.Dequeue();

                if (currentNode.currentBoard == goalState)
                {
                    Console.WriteLine("Solution found");
                    tree.bestPath = buildPath(tree.root, currentNode);
                    Console.WriteLine("NumOfMoves: " + tree.bestPath.Count);
                    PrintPath(tree.bestPath, 10);
                    return;
                }
                else
                {
                    // generate children for current node and move current node to the closed queue
                    // move children to the open queue
                    tree.createChildren(currentNode);
                    if (!tree.closedQueue.Contains(currentNode))
                    {
                        tree.closedQueue.Enqueue(currentNode);
                    }

                    foreach (Node n in currentNode.children)
                    {
                        n.HeuristicScore = currentNode.HeuristicScore + 1;
                        if (!tree.openQueue.Contains(n))
                        {
                            tree.openQueue.Enqueue(n);
                        }
                    }
                }
            }
            Console.WriteLine("Unable to find a solution");
        }

        /// <summary>
        /// Prints the path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="numOfMoves">The number of moves.</param>
        public static String PrintPath(List<Node> path, int rowLength)
        {
            String output = String.Empty;
            while (path.Count >= rowLength)
            {
                output += PrintRows(path, rowLength);
                path.RemoveRange(0, rowLength);
                output += Environment.NewLine;
            }
            if (path.Count > 0)
            {
                output += PrintRows(path, path.Count);
            }
            return output;
        }

        /// <summary>
        /// Prints the rows.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="rowLength">Length of the row.</param>
        public static String PrintRows(List<Node> path, int rowLength)
        {
            String output = String.Empty;
            for (int i = 0; i < rowLength; i++)
            {
                output += path[i].currentBoard.toString(0);
            }
            output += Environment.NewLine;
            for (int i = 0; i < rowLength; i++)
            {
                output += path[i].currentBoard.toString(1);
            }
            output += Environment.NewLine;
            for (int i = 0; i < rowLength; i++)
            {
                output += path[i].currentBoard.toString(2);
            }
            output += Environment.NewLine;
            return output;
        }

        /// <summary>
        /// Builds the path.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="goal">The goal.</param>
        /// <returns></returns>
        public static List<Node> buildPath(Node start, Node goal)
        {
            List<Node> path = new List<Node>();
            Node current = goal;
            while (current != start)
            {
                path.Add(current);
                current = current.parent;
            }
            path.Add(start);
            path.Reverse();
            return path;
        }

    }
}
