﻿using System;
using SharpAdbClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;

namespace PadPlayer
{
    class AndroidController
    {
        /// <summary>
        /// The adb location
        /// </summary>
        private string AdbLocation = Environment.CurrentDirectory + @"\adb.exe";

        /// <summary>
        /// The android screenshot command
        /// </summary>
        private string androidScreenshotCommand = @"screencap -p /mnt/sdcard/output.png";

        /// <summary>
        /// The android remove command
        /// </summary>
        private string androidRemoveCommand = "rm /mnt/sdcard/output.png";

        /// <summary>
        /// The android pull command
        /// </summary>
        private string androidPullCommand = "/mnt/sdcard/output.png";

        /// <summary>
        /// The server
        /// </summary>
        private AdbServer server { get; set; }

        public AndroidController()
        {
            server = new AdbServer();
            try
            {
                server.StartServer(AdbLocation, restartServerIfNewer: false);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        public Bitmap getScreenshotFromAndroidPhone()
        {
            //Connect to device
            var device = AdbClient.Instance.GetDevices().First();
            var receiver = new ConsoleOutputReceiver();

            //Take screenshot on device
            AdbClient.Instance.ExecuteRemoteCommand(androidScreenshotCommand, device, receiver);

            //Transfer to windows
            using (SyncService service = new SyncService(new AdbSocket(AdbServer.Instance.EndPoint), device))
            using (Stream stream = File.OpenWrite(ImageController.screenshotInitial))
            {
                service.Pull(androidPullCommand, stream, null, CancellationToken.None);
                stream.Dispose();
            }

            //Delete on device
            AdbClient.Instance.ExecuteRemoteCommand(androidRemoveCommand, device, receiver);

            return Image.FromFile(ImageController.screenshotInitial) as Bitmap;
        }
    }
}
