﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PadPlayer
{

    public partial class MainForm : Form
    {
        /// <summary>
        /// The android controller
        /// </summary>
        private AndroidController androidController;

        /// <summary>
        /// The image controller
        /// </summary>
        private ImageController imageController;

        /// <summary>
        /// The board tree
        /// </summary>
        private Tree boardTree;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainForm"/> class.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();

            // Fit image to box
            this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            //outputListView.View = View.Details;
            this.outputListView.Columns.Add("Item Column", -2, HorizontalAlignment.Left);
            this.outputListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;

            // Start ADB server for android communication
            this.androidController = new AndroidController();

            // Start the image controller
            this.imageController = new ImageController();

            // Create a new tree
            this.boardTree = new Tree();

            // Set defaults for imagedropdown and search depth
            this.comboBoxHeuristic.SelectedIndex = 0;
            this.comboBoxMinimumCombo.SelectedIndex = 0;
            this.comboBoxColorPriority.SelectedIndex = 0;

            // Set the Minimum, Maximum, and initial Value.
            this.numericUpDownSearchDepth.Value = 5;
            this.numericUpDownSearchDepth.Maximum = 7;
            this.numericUpDownSearchDepth.Minimum = 1;

            this.numericUpDownBoardHeight.Value = 5;
            this.numericUpDownBoardHeight.Maximum = 6;
            this.numericUpDownBoardHeight.Minimum = 4;

            this.numericUpDownBoardWidth.Value = 6;
            this.numericUpDownBoardWidth.Maximum = 7;
            this.numericUpDownBoardWidth.Minimum = 5;

            backgroundWorker1.DoWork += backgroundWorker1_DoWork;
            backgroundWorker1.ProgressChanged += backgroundWorker1_ProgressChanged;
            backgroundWorker1.RunWorkerCompleted += backgroundWorker1_RunWorkerCompleted;  //Tell the user how the process went
            backgroundWorker1.WorkerReportsProgress = true;
        }

        /// <summary>
        /// Adds to output.
        /// </summary>
        /// <param name="value">The value.</param>
        public void AddToOutput(string value)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<string>(this.AddToOutput), new object[] { value });
            }
            else
            {
                ListViewItem row = new ListViewItem();
                row.SubItems.Add(value);
                outputListView.Items.Add(row);
                outputListView.Items[outputListView.Items.Count - 1].EnsureVisible();
            }
        }

        /// <summary>
        /// Adds to output.
        /// </summary>
        /// <param name="value">The value.</param>
        public void AddToOutput(string[] value)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<string[]>(this.AddToOutput), new object[] { value });
            }
            else
            {
                foreach (string s in value)
                {
                    ListViewItem row = new ListViewItem();
                    row.SubItems.Add(s);
                    outputListView.Items.Add(row);
                    outputListView.Items[outputListView.Items.Count - 1].EnsureVisible();
                }
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {


            //If the process exits the loop, ensure that progress is set to 100%
            //Remember in the loop we set i < 100 so in theory the process will complete at 99%
            backgroundWorker1.ReportProgress(100);
        }

        private void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
        }

        /// <summary>
        /// Handles the Click event of the screenshot control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void go_Click(object sender, EventArgs e)
        {
            int oldImage = this.comboBoxImageSelection.SelectedIndex;
            this.comboBoxImageSelection.SelectedIndex = -1;
            imageController.deleteAllScreenshots();
            backgroundWorker1.ReportProgress(5);
            this.comboBoxImageSelection.SelectedIndex = oldImage;

            using (Bitmap screenshot = androidController.getScreenshotFromAndroidPhone())
            {
                imageController.cropScreenshot();
                backgroundWorker1.ReportProgress(10);
                using (Bitmap boardOnly = Image.FromFile(ImageController.screenshotBoardOnly) as Bitmap)
                {
                    Board newBoard = new Board(imageController.buildNewBoardState());
                    backgroundWorker1.ReportProgress(15);
                    imageController.DrawPieceOverlay(newBoard.stateCurrent);
                    backgroundWorker1.ReportProgress(25);
                    boardTree = new Tree(newBoard, HeuristicType.Mark);
 
                    if(Heuristic.StartOnTopOrBottom(newBoard.stateCurrent))
                    {
                        AddToOutput("Chose top");
                    }
                    else
                    {
                        AddToOutput("Chose Bottom");
                    }
                    List<Board> initialMoves = newBoard.generateInitialMoves();
                    backgroundWorker1.ReportProgress(100);
                }
            }

            this.updateImageBox();

            //if (boardTree.bestMovesQueue.Count > 0)
            //{
            //    AddToOutput("Number of Moves: " + numericUpDownSearchDepth.Value);
            //    AddToOutput("Number of Nodes: " + boardTree.numOfNodes);
            //    AddToOutput("Best score found: " + boardTree.bestMovesQueue.ElementAt(0).currentBoard.score);
            //    AddToOutput("Number of Solutions: " + boardTree.bestMovesQueue.Count);
            //    AddToOutput("Path:");
            //    foreach (Point node in boardTree.bestMovesQueue.ElementAt(boardTree.bestMovesQueuePosition).currentBoard.path)
            //    {
            //        AddToOutput("( " + node.X + ", " + node.Y + " )");
            //    }
            //    imageController.drawPath(
            //        boardTree.bestMovesQueue.ElementAt(boardTree.bestMovesQueuePosition).currentBoard.path,
            //        Heuristic.boardWidth,
            //        Heuristic.boardHeight);
            //    updateImageBox();
            //}
        }

        /// <summary>
        /// Updates the image box.
        /// </summary>
        /// <param name="file">The file.</param>
        private void updateImageBox()
        {
            pictureBox1.Image?.Dispose();
            try
            {
                if (this.comboBoxImageSelection.SelectedIndex == 0)
                {
                    pictureBox1.Image = Image.FromFile(ImageController.screenshotBoardOnly);
                }
                else if (this.comboBoxImageSelection.SelectedIndex == 1)
                {
                    pictureBox1.Image = Image.FromFile(ImageController.screenshotPieceOverlay);
                }
                else if (this.comboBoxImageSelection.SelectedIndex == 2)
                {
                    pictureBox1.Image = Image.FromFile(ImageController.screenshotPathOverlay);
                }
                else
                {
                    pictureBox1.Image = null;
                }

                pictureBox1.Refresh();
            }
            catch (Exception e)
            {
                AddToOutput(e.StackTrace);
            }
        }

        /// <summary>
        /// Handles the Click event of the buttonSolutionNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void buttonSolutionNext_Click(object sender, EventArgs e)
        {
            // if(boardTree.bestMovesQueuePosition < boardTree.bestMovesQueue.Count - 1)
            //{
            //    boardTree.bestMovesQueuePosition++;
            //    //UpdateUI();
            //}
        }

        /// <summary>
        /// Handles the Click event of the buttonSolutionBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void buttonSolutionBack_Click(object sender, EventArgs e)
        {
            //if (boardTree.bestMovesQueuePosition > 0)
            //{
            //    boardTree.bestMovesQueuePosition--;
            //    //UpdateUI();
            //}
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the comboBoxImageSelection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void comboBoxImageSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateImageBox();
        }

        /// <summary>
        /// Handles the ValueChanged event of the searchDepthUpDown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void searchDepthUpDown_ValueChanged(object sender, EventArgs e)
        {
            Heuristic.searchDepth = (int)this.numericUpDownSearchDepth.Value;
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the comboBoxHeuristic control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void comboBoxHeuristic_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(this.comboBoxHeuristic.SelectedIndex == 0)
            {
                Heuristic.hueristic = HeuristicType.Mark;
            }
            if (this.comboBoxHeuristic.SelectedIndex == 1)
            {
                Heuristic.hueristic = HeuristicType.Sean;
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the comboBoxMatchStrength control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void comboBoxMinimumCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBoxMinimumCombo.SelectedIndex == 0)
            {
                Heuristic.comboMinimum = 3;
            }
            if (this.comboBoxMinimumCombo.SelectedIndex == 1)
            {
                Heuristic.comboMinimum = 4;
            }
            if (this.comboBoxMinimumCombo.SelectedIndex == 2)
            {
                Heuristic.comboMinimum = 5;
            }
            if (this.comboBoxMinimumCombo.SelectedIndex == 3)
            {
                Heuristic.comboMinimum = 6;
            }
            if (this.comboBoxMinimumCombo.SelectedIndex == 4)
            {
                Heuristic.comboMinimum = 7;
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the comboBox1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void comboBoxColorPriority_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBoxColorPriority.SelectedIndex == 0)
            {
                Heuristic.colorPriority = PieceType.white;
            }
            if (this.comboBoxColorPriority.SelectedIndex == 1)
            {
                Heuristic.colorPriority = PieceType.black;
            }
            if (this.comboBoxColorPriority.SelectedIndex == 2)
            {
                Heuristic.colorPriority = PieceType.red;
            }
            if (this.comboBoxColorPriority.SelectedIndex == 3)
            {
                Heuristic.colorPriority = PieceType.blue;
            }
            if (this.comboBoxColorPriority.SelectedIndex == 4)
            {
                Heuristic.colorPriority = PieceType.green;
            }
            if (this.comboBoxColorPriority.SelectedIndex == 5)
            {
                Heuristic.colorPriority = PieceType.heal;
            }
            if (this.comboBoxColorPriority.SelectedIndex == 6)
            {
                Heuristic.colorPriority = PieceType.jammer;
            }
            if (this.comboBoxColorPriority.SelectedIndex == 7)
            {
                Heuristic.colorPriority = PieceType.poision;
            }
            else
            {
                Heuristic.colorPriority = PieceType.unknown;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numericUpDownBoardWidth_ValueChanged(object sender, EventArgs e)
        {
            ImageController.BoardWidth = (int)this.numericUpDownBoardWidth.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numericUpDownBoardHeight_ValueChanged(object sender, EventArgs e)
        {
            ImageController.BoardHeight = (int)this.numericUpDownBoardHeight.Value;
        }
    }
}