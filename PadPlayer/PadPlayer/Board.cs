﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PadPlayer
{
    #region Enumerations
    public enum PieceType : int
    {
        white = 0,
        black = 1,
        red = 2,
        blue = 3,
        green = 4,
        heal = 5,
        jammer = 6,
        poision = 7,
        unknown = 8,
        count = 9
    }
    #endregion

    public class Board : IDisposable
    {
        /// <summary>
        /// The board height
        /// </summary>
        public int boardHeight { get; set; } = 5;

        /// <summary>
        /// The board width
        /// </summary>
        public int boardWidth { get; set; } = 6;

        /// <summary>
        /// 
        /// </summary>
        public List<Point> path = new List<Point>();

        /// <summary>
        /// 
        /// </summary>
        public double scoreOptimal { get; set; } = 0;

        /// <summary>
        /// 
        /// </summary>
        public double scoreCurrent { get; set; } = 0;

        /// <summary>
        /// List of possible movement piece colors
        /// </summary>
        public Dictionary<PieceType, int> movementPieceOptions;

        /// <summary>
        /// 
        /// </summary>
        public PieceType[,] stateCurrent { get; set; } = null;

        /// <summary>
        /// 
        /// </summary>
        public bool[,] stateFilled { get; set; } = null;

        /// <summary>
        /// 
        /// </summary>
        public bool disposed = false;

        /// <summary>
        /// 
        /// </summary>
        public SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newState"></param>
        /// <param name="currentPath"></param>
        public Board(PieceType[,] newState, List<Point> currentPath = null)
        {
            this.stateCurrent = newState;
            this.path = currentPath ?? new List<Point>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newState"></param>
        /// <param name="currentPoint"></param>
        /// <param name="currentPath"></param>
        public Board(PieceType[,] newState, Point currentPoint, List<Point> currentPath = null)
        {
            this.stateCurrent = newState;
            this.path.Add(currentPoint);
            this.path = currentPath ?? new List<Point>();
            this.movementPieceOptions = new Dictionary<PieceType, int>();
        }

        public Board()
        {
            this.stateCurrent = new PieceType[ImageController.BoardWidth,ImageController.BoardHeight];
            this.scoreOptimal = 0;
            this.movementPieceOptions = new Dictionary<PieceType, int>();
        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="newBoard"></param>
        public Board(Board newBoard)
        {
            this.boardHeight = newBoard.boardHeight;
            this.boardWidth = newBoard.boardWidth;
            this.movementPieceOptions = new Dictionary<PieceType, int>();
            this.path = newBoard.path;
            this.scoreCurrent = newBoard.scoreCurrent;
            this.scoreOptimal = newBoard.scoreOptimal;
            this.stateCurrent = newBoard.stateCurrent;
            this.stateFilled = newBoard.stateFilled;
        }

        /// <summary>
        /// Copies the board.
        /// </summary>
        /// <param name="oldBoard">The old board.</param>
        /// <returns></returns>
        public PieceType[,] getStateCopy()
        {
            PieceType[,] newBoard = new PieceType[this.boardWidth, this.boardHeight];
            for (int i = 0; i < this.boardWidth; i++)
            {
                for (int j = 0; j < this.boardHeight; j++)
                {
                    newBoard[i, j] = this.stateCurrent[i, j];
                }
            }
            return newBoard;

        }

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }

        /// <summary>
        /// Generates the next moves.
        /// </summary>
        /// <param name="state">The board.</param>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public List<Board> generateNextMoves()
        {
            // Make a new list of moves and things to put in it
            List<Board> nextMoves = new List<Board>();

            // Figure out where we are
            Point currentPosition = this.path.ElementAt(this.path.Count - 1);

            // For each piece around us, swap our current piece and save the state
            foreach (Point piece in findSurroundingPieces(currentPosition))
            {
                // Copy current board 
                Board newBoard = new Board();
                newBoard.stateCurrent = this.stateCurrent;
                newBoard.path.Add(currentPosition);

                // Swap with a surrounding piece
                PieceType temp = newBoard.stateCurrent[currentPosition.X, currentPosition.Y];
                newBoard.stateCurrent[currentPosition.X, currentPosition.Y] = this.stateCurrent[piece.X, piece.Y];
                newBoard.stateCurrent[piece.X, piece.Y] = temp;

                // Score new board
                newBoard.scoreOptimal = Heuristic.HeuristicMark(newBoard.stateCurrent);
                nextMoves.Add(newBoard);
                newBoard.Dispose();
            }
            return nextMoves;
        }

        /// <summary>
        /// Finds the surrounding pieces.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <returns></returns>
        private List<Point> findSurroundingPieces(Point position)
        {
            List<Point> points = new List<Point>();

            for (int x = Math.Max(0, position.X - 1); x <= Math.Min(position.X + 1, boardWidth); x++)
            {
                for (int y = Math.Max(0, position.Y - 1); y <= Math.Min(position.Y + 1, boardHeight); y++)
                {
                    if (x >= 0 && y >= 0 && x < boardWidth && y < boardHeight)
                    {
                        if (x != position.X || y != position.Y)
                        {
                            points.Add(new Point(x, y));
                        }
                    }
                }
            }

            return points;
        }

        /// <summary>
        /// Generates the initial moves.
        /// </summary>
        /// <param name="state">The starting board.</param>
        /// <returns></returns>
        public List<Board> generateInitialMoves()
        {
            List<Board> initialMoves = new List<Board>();
            List<Point> localPath;
            for (int i = 0; i < boardWidth; i++)
            {
                for (int j = 0; j < boardHeight; j++)
                {
                    localPath = new List<Point>();
                    localPath.Add(new Point(i, j));
                    this.path = localPath;
                    foreach (Board move in generateNextMoves())
                    {
                        initialMoves.Add(move);
                    }
                }
            }
            return initialMoves;
        }

        /// <summary>
        /// Prints to the console, any given state
        /// </summary>
        /// <param name="currentState"></param>
        public String toString()
        {
            String output = String.Empty;
            for (int i = 0; i < boardWidth; i++)
            {
                for (int j = 0; j < boardHeight; j++)
                {
                    output += this.stateCurrent[i, j] + " ";
                }
            }
            return output;
        }

        /// <summary>
        /// Prints out any specific row of a state
        /// </summary>
        /// <param name="row">The row to print</param>
        public String toString(int row)
        {
            String output = String.Empty;
            for (int i = 0; i < boardWidth; i++)
            {
                output += this.stateCurrent[i, row] + " ";
            }
            output += "   ";
            return output;
        }
    }
}
