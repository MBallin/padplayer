﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PadPlayer
{
    class ImageController
    {
        /// <summary>
        /// The screenshot initial
        /// </summary>
        public static string screenshotInitial { get; set; } = Environment.CurrentDirectory + @"\screenshot.png";

        /// <summary>
        /// The screenshot cropped
        /// </summary>
        public static string screenshotBoardOnly { get; set; } = Environment.CurrentDirectory + @"\boardOnly.png";

        /// <summary>
        /// The screenshot overlay
        /// </summary>
        public static string screenshotPieceOverlay { get; set; } = Environment.CurrentDirectory + @"\boardWithOverlay.png";

        /// <summary>
        /// The screenshot path
        /// </summary>
        public static string screenshotPathOverlay { get; set; } = Environment.CurrentDirectory + @"\boardWithPath.png";

        /// <summary>
        /// 
        /// </summary>
        public static int BoardWidth { get; set; } = 0;

        /// <summary>
        /// 
        /// </summary>
        public static int BoardHeight { get; set; } = 0;

        /// <summary>
        /// The pen white
        /// </summary>
        private Pen penWhite;

        /// <summary>
        /// The pen black
        /// </summary>
        private Pen penBlack;

        /// <summary>
        /// The pen red
        /// </summary>
        private Pen penRed;

        /// <summary>
        /// The pen green
        /// </summary>
        private Pen penGreen;

        /// <summary>
        /// The pen blue
        /// </summary>
        private Pen penBlue;

        /// <summary>
        /// The pen heal
        /// </summary>
        private Pen penHeal;

        /// <summary>
        /// The pen jammer
        /// </summary>
        private Pen penJammer;

        /// <summary>
        /// The pen poison
        /// </summary>
        private Pen penPoison;

        /// <summary>
        /// The pen unknown
        /// </summary>
        private Pen penUnknown;

        /// <summary>
        /// Gets or sets the circle x.
        /// </summary>
        /// <value>
        /// The circle x.
        /// </value>
        public int pieceCenterOffsetX { get; set; } = 0;

        /// <summary>
        /// 
        /// </summary>
        public int pieceCenterOffsetY { get; set; } = 0;

        /// <summary>
        /// 
        /// </summary>
        public int PieceHeight { get; set; } = 155;

        /// <summary>
        /// 
        /// </summary>
        public int PieceWidth { get; set; } = 155;

        /// <summary>
        /// Converstion from type of piece to pen color.
        /// </summary>
        private Dictionary<PieceType,Pen> PieceTypeToPen;

        public ImageController()
        {
            // Initalize pens
            this.penWhite = new Pen(Color.White, 2);
            this.penWhite.Alignment = PenAlignment.Inset;
            this.penBlack = new Pen(Color.Black, 2);
            this.penBlack.Alignment = PenAlignment.Inset;
            this.penRed = new Pen(Color.Red, 2);
            this.penRed.Alignment = PenAlignment.Inset;
            this.penGreen = new Pen(Color.DarkGreen, 2);
            this.penGreen.Alignment = PenAlignment.Inset;
            this.penBlue = new Pen(Color.DarkBlue, 2);
            this.penBlue.Alignment = PenAlignment.Inset;
            this.penHeal = new Pen(Color.LightPink, 2);
            this.penHeal.Alignment = PenAlignment.Inset;
            this.penJammer = new Pen(Color.SandyBrown, 2);
            this.penJammer.Alignment = PenAlignment.Inset;
            this.penPoison = new Pen(Color.PaleGreen, 2);
            this.penPoison.Alignment = PenAlignment.Inset;
            this.penUnknown = new Pen(Color.Black, 2);
            this.penUnknown.Alignment = PenAlignment.Inset;
            this.PieceTypeToPen = new Dictionary<PieceType, Pen>()
            {
                {PieceType.black, penBlack },
                {PieceType.blue , penBlue },
                {PieceType.green, penGreen },
                {PieceType.heal, penHeal },
                {PieceType.jammer, penJammer },
                {PieceType.poision, penPoison },
                {PieceType.red, penRed },
                {PieceType.unknown, penUnknown},
                {PieceType.white, penWhite }
            };
        }

        /// <summary>
        /// Gets the pen from piece.
        /// </summary>
        /// <param name="piece">The piece.</param>
        /// <returns></returns>
        public Pen getPenFromPiece(PieceType piece)
        {
            Pen selectedPen = PieceTypeToPen[piece];
            if (selectedPen != penUnknown)
            {
                selectedPen.Width = 15;
            }
            else
            {
                selectedPen.Width = 5;
            }
            return selectedPen;
        }

        /// <summary>
        /// Generates the overlay and board.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        public Board buildNewBoardState()
        {
            Board newBoard = new Board();
            using (Bitmap source = Image.FromFile(screenshotBoardOnly) as Bitmap)
            {
                this.PieceHeight = source.Height / BoardHeight;
                this.PieceWidth = source.Width / BoardWidth;
                try
                {
                    for (int i = 0; i < BoardWidth; i++)
                    {
                        for (int j = 0; j < BoardHeight; j++)
                        {
                            Point positionToCheck = new Point(this.pieceCenterOffsetX + (PieceHeight / 6), this.pieceCenterOffsetY + (PieceWidth / 3));
                            PieceType piece = takeSample(source, positionToCheck);
                            if(newBoard.movementPieceOptions.ContainsKey(piece))
                            {
                                newBoard.movementPieceOptions[piece]++;
                            }
                            else
                            {
                                newBoard.movementPieceOptions.Add(piece, 1);
                            }
                            newBoard.stateCurrent[i, j] = piece;
                            Console.WriteLine("Piece: (" + i + ", " + j + ") " + piece.ToString());
                            this.pieceCenterOffsetY += PieceHeight;
                        }
                        this.pieceCenterOffsetY = 0;
                        this.pieceCenterOffsetX += PieceWidth;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                }
            }
            return newBoard;
        }

        /// <summary>
        /// Gets the screenshot.
        /// </summary>
        public bool cropScreenshot()
        {
            // Crop board from image
            int cropX = 50;
            int cropY = 965;
            int cropWidth = 975;
            int cropHeight = 810;
            Rectangle croppedRec = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap target = new Bitmap(croppedRec.Width, croppedRec.Height);

            try
            {
                using (Bitmap source = Image.FromFile(screenshotInitial) as Bitmap)
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.DrawImage(source,
                        new Rectangle(0, 0, target.Width, target.Height),
                        croppedRec,
                        GraphicsUnit.Pixel);
                }
                target.Save(screenshotBoardOnly, System.Drawing.Imaging.ImageFormat.Bmp);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Deletes all screenshots.
        /// </summary>
        public bool deleteAllScreenshots()
        {
            return deleteScreenshot(screenshotInitial) && deleteScreenshot(screenshotBoardOnly) && deleteScreenshot(screenshotPieceOverlay) && deleteScreenshot(screenshotPathOverlay);
        }

        /// <summary>
        /// Deletes all screenshots.
        /// </summary>
        public bool deleteScreenshot(string image)
        {
            try
            {
                if(image.Equals(screenshotBoardOnly))
                {
                    File.Delete(screenshotBoardOnly);
                }
                if (image.Equals(screenshotInitial))
                {
                    File.Delete(screenshotInitial);
                }
                if (image.Equals(screenshotPieceOverlay))
                {
                    File.Delete(screenshotPieceOverlay);
                }
                if (image.Equals(screenshotPathOverlay))
                {
                    File.Delete(screenshotPathOverlay);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Draws the path.
        /// </summary>
        /// <param name="path">The path.</param>
        public bool drawPath(List<Point> path, int boardWidth, int boardHeight)
        {
            try
            {
                using (Bitmap src = Image.FromFile(screenshotBoardOnly) as Bitmap)
                using (Graphics g = Graphics.FromImage(src))
                {
                    pieceCenterOffsetX = 0;
                    pieceCenterOffsetY = 0;
                    for (int i = 0; i < path.Count - 1; i++)
                    {
                        if (i == 0)
                        {
                            g.FillEllipse(
                                new SolidBrush(Color.Red),
                                new Rectangle(
                                    (path.ElementAt(i).X * (src.Width / boardWidth) + 60),
                                    (path.ElementAt(i).Y * (src.Height / boardHeight) + 60),
                                    60,
                                    60));
                        }

                        g.DrawLine(
                            penWhite,
                            (path.ElementAt(i).X * (src.Width / boardWidth) + 60),
                            (path.ElementAt(i).Y * (src.Height / boardHeight) + 60),
                            (path.ElementAt(i + 1).X * (src.Width / boardWidth) + 60),
                            (path.ElementAt(i + 1).Y * (src.Height / boardHeight) + 60));
                    }
                    src.Save(screenshotPathOverlay, System.Drawing.Imaging.ImageFormat.Bmp);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Generates the overlay and board.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        public bool DrawPieceOverlay(PieceType[,] state)
        {
            try
            {
                using (Bitmap src = Image.FromFile(screenshotBoardOnly) as Bitmap)
                using (Graphics g = Graphics.FromImage(src))
                {
                    pieceCenterOffsetX = 0;
                    pieceCenterOffsetY = 0;

                    for (int i = 0; i < state.GetLength(0); i++)
                    {
                        for (int j = 0; j < state.GetLength(1); j++)
                        {
                            Pen selectedPen = getPenFromPiece(state[i, j]);
                            g.DrawEllipse(selectedPen, pieceCenterOffsetX, pieceCenterOffsetY, PieceHeight, PieceWidth);
                            g.DrawEllipse(penRed, pieceCenterOffsetX + (PieceHeight / 6), pieceCenterOffsetY + (PieceWidth / 3), 20, 20);
                            pieceCenterOffsetY += PieceWidth;
                        }
                        pieceCenterOffsetY = 0;
                        pieceCenterOffsetX += PieceWidth;
                    }
                    src.Save(screenshotPieceOverlay, System.Drawing.Imaging.ImageFormat.Bmp);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
            return true;
        }
           
        /// <summary>
        /// Takes the sample.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="position">The position.</param>
        /// <returns></returns>
        public PieceType takeSample(Bitmap source, Point position)
        {
            int[] average = new int[3] { 0, 0, 0 };
            // Subsample
            for (int i = -5; i < 5; i++)
            {
                for (int j = -5; j < 5; j++)
                {
                    Color Pixel = source.GetPixel(position.X + i, position.Y + i);
                    average[0] += Pixel.R;
                    average[1] += Pixel.G;
                    average[2] += Pixel.B;
                }
            }
            average[0] /= 100;
            average[1] /= 100;
            average[2] /= 100;
            //AddToOutput("r = " + average[0] + ", g = " + average[1] + ", b = " + average[2]);

            return getPieceTypeFromSample(Color.FromArgb(average[0], average[1], average[2]));
        }

        /// <summary>
        /// Gets the piece type from sample.
        /// </summary>
        /// <param name="Pixel">The pixel.</param>
        /// <returns></returns>
        public PieceType getPieceTypeFromSample(Color Pixel)
        {
            if (Pixel.R > 200 && Pixel.G < 100 && Pixel.B < 100)
            {
                return PieceType.red;
            }
            else if (Pixel.R <= 50 && Pixel.G <= 50 && Pixel.B <= 50)
            {
                return PieceType.jammer;
            }
            else if (Pixel.R >= 230 && Pixel.B >= 200)
            {
                return PieceType.heal;
            }
            else if ((Pixel.R <= 170 || Pixel.R >= 200) && (Pixel.G <= 125 || Pixel.G >= 230) && Pixel.B >= 150)
            {
                return PieceType.black;
            }
            else if (Pixel.R > 180 && (Pixel.G < 100 || Pixel.G >= 175) && Pixel.B >= 80)
            {
                return PieceType.white;
            }
            else if (Pixel.R < Pixel.G && Pixel.R < Pixel.B && Pixel.G < Pixel.B)
            {
                return PieceType.blue;
            }
            else if (Pixel.R < Pixel.G && Pixel.R < Pixel.B && Pixel.G > Pixel.B)
            {
                return PieceType.green;
            }
            else if ((Pixel.R - Pixel.G) <= 25 && Pixel.B - Pixel.G <= 25)
            {
                return PieceType.poision;
            }
            return PieceType.unknown;
        }
    }
}
